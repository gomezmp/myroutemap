//
//  PointInformation.swift
//  MyRouteMap
//
//  Created by Maria Paula Gómez on 11/30/19.
//  Copyright © 2019 Mapis. All rights reserved.
//
import Foundation

struct PointInformation {
    var latitude: Double
    var longitude: Double
    var name: String
}
