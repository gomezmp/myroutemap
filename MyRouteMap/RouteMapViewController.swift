//
//  RouteMapViewController.swift
//  MyRouteMap
//
//  Created by Maria Paula Gómez on 11/30/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import UIKit
import GoogleMaps

class RouteMapViewController: UIViewController {

    // MARK: - UI References
    
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    // MARK: - Properties
    
    private var locationManager = CLLocationManager()
    private var countMarker = 0
    private var lastLocation: CLLocation?
    private let originPoint = PointInformation(latitude: 4.665154, longitude: -74.057948, name: "Unilago")
    private let endPoint = PointInformation(latitude: 4.672655, longitude: -74.054071, name: "Parque Virrey")
    
     // MARK: - cycle life
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }

     // MARK: - Private methods
    
    private func setupUI(){
        locationManager.requestAlwaysAuthorization()
        speedLabel.layer.cornerRadius = 40
        speedLabel.layer.masksToBounds = true
        
        if CLLocationManager.locationServicesEnabled() {
           locationManager.delegate = self
           locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
           locationManager.startUpdatingLocation()
        }
        
        showLine()
    }

    /**
      This function draws the polyline from the point of origin to the end
    */
    private func showLine(){
       addMarket(latitude: originPoint.latitude, longitude: originPoint.longitude, color: .blue, title: originPoint.name)
       addMarket(latitude: endPoint.latitude, longitude: endPoint.longitude, color: .blue, title: endPoint.name)
       let path = GMSMutablePath()
        path.add(CLLocationCoordinate2DMake(originPoint.latitude, originPoint.longitude))
       path.add(CLLocationCoordinate2DMake(endPoint.latitude, endPoint.longitude))
       let line = GMSPolyline(path: path)
       line.strokeColor = UIColor.blue
       line.strokeWidth = 3.0
       line.map = mapView
    }
    
    /**
      This function draws a market
       - latitude:  is the latitud of the market point
       - longitude:  is the longitude of the market point
       - color:  is the color of the market point
       - title:  is the title of the market point
    */
    private func addMarket(latitude: Double, longitude: Double, color: UIColor, title: String){
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        marker.icon = GMSMarker.markerImage(with: color)
        marker.title = title
        marker.map = mapView
    }
}

extension RouteMapViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        mapView.camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 16.0)
        
        if let speed = manager.location?.speed{
            speedLabel.isHidden = false
            speedLabel.text = "\(speed) m/s"
        }
        
        if let oldLocation = lastLocation ,let distanceMeters = (manager.location?.distance(from: oldLocation)) , distanceMeters >= 1.5 {
            countMarker = countMarker + 1
            addMarket(latitude: locValue.latitude, longitude: locValue.longitude, color: .red, title:"Punto: \(countMarker)")
        }else if lastLocation == nil{
            addMarket(latitude: locValue.latitude, longitude: locValue.longitude, color: .red, title:"Origen")
            lastLocation = manager.location
        }
        
    }
}

